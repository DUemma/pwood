
# coding: utf-8

# ## Basic Photochemical Cycle of  NO2
# 
# $$ NO_2 + hv \xrightarrow\ NO + O $$
# $$ O + O_2 + M \xrightarrow\ O_3 + M $$
# 
# When NO and NO2 are present in sunlight, ozone formation occurs. M represents N2 or O2, molecule that absorbs excess energy to stabilize the O3 molecule. Then, O3 reacts with NO to regenerate NO2. 
# 
# $$ O_3 + NO \xrightarrow\ NO_2 + O_2 $$
# 
# Rate of change of concentration of NO2 after irradiation is:
# 
# $$ \frac{d[NO_2]}{dt} = -j_{5.1}{NO_2}+k_{5.3}{O_3}{NO} $$
# 
# Equation for O:
# 
# $$ \frac{d[O]}{dt} = -j_{5.1}{NO_2}+k_{5.2}{O}\times {O_2}\times {M} $$
# 
# 
# 
# 

# In[32]:


import numpy as np
import matplotlib.pyplot as plt
from scipy.integrate import odeint

# Constants (found in S+P 5.1)
### concentration constants in trophosphere
M = 7.1e22
#Composition of NO in atmosphere
NO = .00078*M
#Composition of O3 in atmosphere
O3 = .0001*M
#Composition of O2 in atmosphere
O2 = .21*M


### rate constants ratio of j5.1/k5.3 is 10 ppb (10e-9)
j51 = 1e-16 #photodissociation rate, assumed
k53 = 1e-8
k52 = 3e-4

# Initial conditions
### guess based on the concentration of molecules in the atmosphere
initial_state = [1e-10*M,1e-10*M]

# Set the number of iterations and time period for simulation 
odeIterations = 1000
endtime = 3600*24*5
timeperiod = np.linspace(0.0, endtime, odeIterations)    


# Definition of system of ordinary differential equations if initial NO and NO2 are 0
def deriv(conc, t):
     NO2 = conc[0]
     O = conc[1] 
     conc = [-j51*NO2+k53*O3*NO, j51*NO2-k52*O*O2*M] #O conc is close to 0
     return conc

# Use of scipy.integrate to function to solve 
sol = odeint(deriv, initial_state, timeperiod,mxstep=10000)

# Steady state value 
### Derived analytically considering the values of the rate constants and relative concentrations
# steady state oxygen levels

Ossval=(j51*100000)/(k52*O2*M) #wrong
Oss = np.ones(len(timeperiod))*Ossval

# Plot
plt.semilogy(timeperiod, sol[:,0], 'b', label='[NO2](t)')
plt.semilogy(timeperiod, sol[:,1], 'g', label='[$O$](t)')
plt.semilogy(timeperiod, Oss,'r',label='[$O$]$_{ss}$')
plt.legend(loc='best')
plt.title('Photochemical Cycle of NO2')
plt.xlabel('time (s)')
plt.ylabel('concentration (molecules cm$^{-3}$)')
plt.grid()
plt.show()


# In[ ]:




# In[ ]:



